import React from 'react';
import {Text, TouchableOpacity, StatusBar, View} from 'react-native';
import {Container, Header, Left, Body, Icon, Content, Right} from 'native-base';
import styles from '../assets/styles/WorkPlaceStyle';
import {Actions} from 'react-native-router-flux';
import {ProgressCircle} from 'react-native-svg-charts';

class WorkPlace extends React.Component {
  render() {
    return (
      <Container style={styles.Background}>
        <Header style={styles.Header} androidStatusBarColor="#fbfbfb">
          <StatusBar barStyle="dark-content" />
          <Left style={styles.LeftBtn}>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon name="md-arrow-round-back" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={styles.HeaderText}>Office Assistant</Text>
          </Body>
          <Right style={styles.RightBtn}>
            <Icon name="more" />
          </Right>
        </Header>
        <Content style={styles.Content}>
          <TouchableOpacity style={styles.TaskWrapper}>
            <View style={styles.Task}>
              <View style={styles.TaskTitle}>
                <Text numberOfLines={1} style={styles.TaskTitleText}>
                  My HomeWork
                </Text>
              </View>
              <View style={styles.Time}>
                <View>
                  <ProgressCircle
                    style={styles.ProgressCircle}
                    progress={0.8}
                    progressColor={'#468ee1'}>
                    <Icon style={styles.CheckIcon} name="ios-checkmark" />
                  </ProgressCircle>
                </View>
                <Text style={styles.TimeText}>04:21</Text>
              </View>
            </View>
            <View style={styles.Important_H} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.TaskWrapper}>
            <View style={styles.Task}>
              <View style={styles.TaskTitle}>
                <Text numberOfLines={1} style={styles.TaskTitleText}>
                  Fix the Car
                </Text>
              </View>
              <View style={styles.Time}>
                <View>
                  <ProgressCircle
                    style={styles.ProgressCircle}
                    progress={0.1}
                    progressColor={'#468ee1'}>
                    <Icon style={styles.CheckIcon} name="ios-checkmark" />
                  </ProgressCircle>
                </View>
                <Text style={styles.TimeText}>03:51</Text>
              </View>
            </View>
            <View style={styles.Important_M} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.TaskWrapper}>
            <View style={styles.Task}>
              <View style={styles.TaskTitle}>
                <Text numberOfLines={1} style={styles.TaskTitleText}>
                  CarWash
                </Text>
              </View>
              <View style={styles.Time}>
                <View>
                  <ProgressCircle
                    style={styles.ProgressCircle}
                    progress={0.3}
                    progressColor={'#468ee1'}>
                    <Icon style={styles.CheckIcon} name="ios-checkmark" />
                  </ProgressCircle>
                </View>
                <Text style={styles.TimeText}>00:00</Text>
              </View>
            </View>
            <View style={styles.Important_L} />
          </TouchableOpacity>
        </Content>
        <TouchableOpacity
          onPress={() => Actions.push('addTask')}
          style={styles.AddTaskBtn}>
          <Text style={styles.AddTaskBtnText}>+</Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default WorkPlace;
