import React from 'react';
import {Text, Image, TouchableOpacity, StatusBar} from 'react-native';
import {Container, View} from 'native-base';
import styles from '../assets/styles/HomeStyle';
import {Actions} from 'react-native-router-flux';

class Home extends React.Component {
  render() {
    return (
      <Container style={styles.Background}>
        <StatusBar barStyle="dark-content" backgroundColor="#fbfbfb" />
        <Image
          resizeMode="contain"
          source={require('./../assets/images/design.png')}
          style={styles.ImageHome}
        />
        <View>
          <Text style={styles.sectionTitle}>Reminder</Text>
          <Text style={styles.sectionDescription}>
            It is a long established fact that a reader will be distracted by
            the readable content of a page when looking at its layout. The point
            of
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => Actions.push('workPlace')}
          style={styles.ButtonHome}>
          <Text style={styles.TextButton}>Lets get started!</Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default Home;
