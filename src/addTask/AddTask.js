import React from 'react';
import {Text, StatusBar, TouchableOpacity, View} from 'react-native';
import {
  Container,
  Content,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Form,
  Label,
  Item,
  Input,
} from 'native-base';
import styles from '../assets/styles/AddTaskStyle';
import {Actions} from 'react-native-router-flux';
import Modal from 'react-native-modal';

class AddTask extends React.Component {
  state = {
    Done: false,
  };
  render() {
    return (
      <Container style={styles.Background}>
        <Header style={styles.Header} androidStatusBarColor="#fbfbfb">
          <StatusBar barStyle="dark-content" />
          <Left style={styles.LeftBtn}>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon name="md-arrow-round-back" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={styles.HeaderText}>Add New</Text>
          </Body>
          <Right style={styles.RightBtn}>
            <Icon name="more" />
          </Right>
        </Header>
        <Content>
          <Form>
            <Item
              style={{
                padding: 5,
                marginBottom: 5,
                borderBottomColor: 'transparent',
              }}
              floatingLabel>
              <Label>Title :</Label>
              <Input />
            </Item>
            <Item
              style={{
                padding: 5,
                marginBottom: 5,
                borderBottomColor: 'transparent',
              }}
              floatingLabel>
              <Label>DeadLine :</Label>
              <Input />
            </Item>
            <Item
              style={{
                padding: 5,
                marginBottom: 5,
                borderBottomColor: 'transparent',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  flex: 1,
                }}>
                <Text>Importance ?</Text>
                <Icon name="arrow-dropdown" />
              </View>
            </Item>
          </Form>
        </Content>
        <Modal
          onBackButtonPress={() =>
            this.setState({Done: false}, () => Actions.pop())
          }
          backdropColor="#fff"
          backdropOpacity={1}
          isVisible={this.state.Done}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#fff',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{alignItems: 'center'}}>
              <View
                style={{
                  borderColor: '#468ee1',
                  borderWidth: 6,
                  width: 100,
                  height: 100,
                  borderRadius: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 20,
                }}>
                <Icon
                  style={{fontSize: 50, color: '#468ee1'}}
                  name="checkmark"
                />
              </View>
              <Text style={{fontSize: 24}}>You're All Set</Text>
            </View>
          </View>
        </Modal>
        <TouchableOpacity
          onPress={() => this.setState({Done: true})}
          style={styles.AddBtn}>
          <Text style={styles.AddText}>Add New Item</Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default AddTask;
