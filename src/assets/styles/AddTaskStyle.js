import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  Header: {
    backgroundColor: '#fbfbfb',
    elevation: 0,
  },
  HeaderText: {
    fontSize: 18,
  },
  LeftBtn: {
    marginLeft: 10,
  },
  RightBtn: {
    marginRight: 10,
  },
  CheckIcon: {
    fontSize: 50,
    marginTop: 10,
    color: '#ede9ef',
    alignSelf: 'center',
  },
  Background: {
    backgroundColor: '#fbfbfb',
  },
  Content: {
    padding: 10,
  },
  AddBtn: {
    padding: 15,
    backgroundColor: '#e24a4a',
    marginHorizontal: '10%',
    borderRadius: 5,
    marginBottom: 20,
  },
  AddText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
  },
});
export default styles;
