import {Colors} from 'react-native/Libraries/NewAppScreen';
import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    textAlign: 'center',
    fontSize: 30,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    textAlign: 'center',
    fontSize: 15,
    color: '#746d76',
    paddingHorizontal: 30,
    marginTop: 10,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  Background: {
    backgroundColor: '#fbfbfb',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  ImageHome: {
    width: '80%',
    height: '40%',
    alignSelf: 'center',
  },
  ButtonHome: {
    width: Width * 0.8,
    backgroundColor: '#e24a4a',
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
  },
  TextButton: {
    fontSize: 18,
    fontWeight: '400',
    color: Colors.white,
  },
});
export default styles;
