import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  AddTaskBtn: {
    backgroundColor: '#31b6a5',
    padding: 10,
    height: 60,
    width: 60,
    borderRadius: 30,
    position: 'absolute',
    bottom: 30,
    right: 30,
    alignItems: 'center',
    elevation: 6,
    justifyContent: 'center',
  },
  ProgressCircle: {
    height: 70,
    width: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  AddTaskBtnText: {
    fontSize: 36,
    color: 'white',
  },
  Header: {
    backgroundColor: '#fbfbfb',
    elevation: 0,
  },
  HeaderText: {
    fontSize: 18,
  },
  LeftBtn: {
    marginLeft: 10,
  },
  chart: {
    height: 70,
    width: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35,
    borderColor: '#468ee1',
    borderWidth: 6,
  },
  RightBtn: {
    marginRight: 10,
  },
  CheckIcon: {
    fontSize: 50,
    marginTop: 10,
    color: '#ede9ef',
    alignSelf: 'center',
  },
  TimeText: {
    marginTop: 10,
  },
  TaskTitle: {
    flex: 1,
  },
  TaskTitleText: {
    fontSize: 20,
  },
  TaskWrapper: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 15,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 3,
    alignItems: 'center',
  },
  Important_H: {
    width: '90%',
    height: 10,
    backgroundColor: '#e24a4a',
    borderBottomStartRadius: 5,
    borderBottomEndRadius: 5,
    alignSelf: 'flex-start',
  },
  Important_L: {
    width: '30%',
    height: 10,
    backgroundColor: 'green',
    borderBottomStartRadius: 5,
    borderBottomEndRadius: 5,
    alignSelf: 'flex-start',
  },
  Important_M: {
    width: '65%',
    height: 10,
    backgroundColor: '#e1ed29',
    borderBottomStartRadius: 5,
    borderBottomEndRadius: 5,
    alignSelf: 'flex-start',
  },
  Task: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
  },
  Time: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  Background: {
    backgroundColor: '#fbfbfb',
  },
  Content: {
    padding: 10,
  },
});
export default styles;
