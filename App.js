/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {BackHandler, ToastAndroid, I18nManager} from 'react-native';
import {Router, Scene, Actions, Stack} from 'react-native-router-flux';

import {connect, Provider} from 'react-redux';
import RNRestart from 'react-native-restart';
import Home from './src/home/Home';
import store from './src/redux/store/index';
import WorkPlace from './src/workPlace.js/WorkPlace';
import AddTask from './src/addTask/AddTask';

let backButtonPressedOnceToExit = false;

class App extends React.Component {
  constructor(props) {
    super(props);

    I18nManager.allowRTL(false);

    if (I18nManager.isRTL) {
      RNRestart.Restart();
    }
  }

  onBackPress() {
    if (backButtonPressedOnceToExit) {
      BackHandler.exitApp();
    } else {
      if (Actions.currentScene !== 'home') {
        Actions.pop();
        return true;
      } else {
        backButtonPressedOnceToExit = true;
        ToastAndroid.show(
          'برای خروج از برنامه دوباره دکمه بازگشت را بفشارید.',
          ToastAndroid.SHORT,
        );
        //setting timeout is optional
        setTimeout(() => {
          backButtonPressedOnceToExit = false;
        }, 2000);
        return true;
      }
    }
  }

  render() {
    const RouterWithRedux = connect()(Router);

    return (
      <Provider store={store}>
        <RouterWithRedux backAndroidHandler={this.onBackPress.bind(this)}>
          <Stack hideNavBar>
            <Scene key="root" hideNavBar>
              <Scene key="home" component={Home} />
              <Scene key="workPlace" component={WorkPlace} />
              <Scene key="addTask" component={AddTask} />
            </Scene>
          </Stack>
        </RouterWithRedux>
      </Provider>
    );
  }
}

export default App;
